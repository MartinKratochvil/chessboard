package cz.nitram.chessboard;

import java.util.InputMismatchException;
import java.util.Scanner;

public class chessboard {

    private static final char whiteChar = '\u2591';

    private static final char blackChar = '\u2588';

    private static final char borderDiagonal = '\u2503';

    private static final char borderHorizontal = '\u2501';

    private static final char borderCorner = '\u254b';

    private static byte boardSize;

    private static byte fieldSize;


    public static void main(String[] args) {
        try
        {
            Scanner scanner = new Scanner(System.in);

            System.out.println("šířka pole:");
            fieldSize = scanner.nextByte();

            System.out.println("šířka desky:");
            boardSize = scanner.nextByte();

            if (boardSize <= 0 || fieldSize <= 0)
            {
                throw new NumberFormatException();
            }

            createBoard();
        }
        catch (InputMismatchException e)
        {
            System.out.println("Nezadal jsi short!!");
        }
        catch (NumberFormatException e)
        {
            System.out.println("Zadal jsi zápornou hodnotu!!");
        }
    }


    private static void createBoard()
    {
        StringBuilder board = new StringBuilder(horizontalBorder());

        for (int i = 0; i < boardSize; i++)
        {
            board.append(createLine(i % 2 == 0).repeat(fieldSize));
        }
        board.append(horizontalBorder());

        System.out.println(board);
    }


    private static String createLine(boolean isWhite)
    {
        StringBuilder boardLine = new StringBuilder(Character.toString(borderDiagonal));

        for (int i = 0; i < boardSize; i++)
        {
            boardLine.append(fieldCharHolder(i, isWhite).repeat(fieldSize));
        }
        boardLine.append(borderDiagonal).append(System.lineSeparator());

        return  boardLine.toString();
    }


    private static String fieldCharHolder(int i, boolean isWhite)
    {
        return (i % 2 == 0 ^ isWhite) ? Character.toString(blackChar) : Character.toString(whiteChar);
    }


    private static String horizontalBorder()
    {
        return borderCorner + Character.toString(borderHorizontal).repeat(boardSize * fieldSize) + borderCorner + System.lineSeparator();
    }
}
